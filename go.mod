module dp-pageviews

go 1.15

require (
	gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang v0.0.0-20220322011350-df509f780b5c
	github.com/albertogviana/prometheus-middleware v0.0.2
	github.com/bitly/go-hostpool v0.1.0 // indirect
	github.com/go-openapi/jsonreference v0.20.2 // indirect
	github.com/go-openapi/spec v0.20.9 // indirect
	github.com/go-openapi/swag v0.22.4 // indirect
	github.com/gocql/gocql v1.5.2
	github.com/golang/snappy v0.0.4 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/klauspost/compress v1.16.7 // indirect
	github.com/prometheus/client_golang v1.16.0
	github.com/prometheus/common v0.44.0 // indirect
	github.com/prometheus/procfs v0.11.0 // indirect
	github.com/stretchr/testify v1.8.4
	github.com/swaggo/swag v1.16.1
	github.com/tj/assert v0.0.3 // indirect
	github.com/valyala/fasthttp v1.48.0 // indirect
	gitlab.wikimedia.org/frankie/aqsassist v0.0.0-20230714043759-9a5f526e90e1
	golang.org/x/sys v0.10.0 // indirect
	golang.org/x/tools v0.10.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
	gopkg.in/yaml.v2 v2.4.0
	schneider.vip/problem v1.8.1
)
