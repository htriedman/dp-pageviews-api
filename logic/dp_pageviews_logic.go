package logic

import (
	"context"
	"dp-pageviews/data"
	"dp-pageviews/entities"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"gitlab.wikimedia.org/frankie/aqsassist"
	"schneider.vip/problem"
)

type DPPageviewsLogicInterface interface {
	ProcessDPPageviewsLogic(context context.Context, reqUrl, project, country_code, year, month, day, page_id string, session *gocql.Session, rLogger *logger.RequestScopedLogger) (*problem.Problem, entities.DPPageviewsResponse)
}

type DPPageviewsLogic struct {
	Data data.DPPageviewsDataInterface
}

func NewDPPageviewsLogic(data data.DPPageviewsDataInterface) DPPageviewsLogic {
	return DPPageviewsLogic{Data: data}
}

func (s DPPageviewsLogic) ProcessDPPageviewsLogic(context context.Context, reqUrl, project, country_code, year, month, day, page_id string, session *gocql.Session, rLogger *logger.RequestScopedLogger) (*problem.Problem, entities.DPPageviewsResponse) {
	var err error
	var problemData *problem.Problem
	var response = entities.DPPageviewsResponse{Items: make([]entities.DPPageviews, 0)}
	var delta, epsilon, noise_scale, noise_type, release_thresh string
	var views int
	scanner := s.Data.ProcessDPPageviewsQuery(context, project, country_code, year, month, day, page_id, session, rLogger)
	for scanner.Next() {
		if err = scanner.Scan(&delta, &epsilon, &noise_scale, &noise_type, &release_thresh, &views); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateProblem(http.StatusNotFound, "The date(s) you used are valid, but we either do "+
					"not have data for those date(s), or the project you asked for is not loaded yet.  "+
					"Please check documentation for more information.", reqUrl)
				return problemResp, entities.DPPageviewsResponse{}
			} else {
				rLogger.Log(logger.ERROR, "Query failed: %s", err)
				problemResp := aqsassist.CreateProblem(http.StatusInternalServerError, err.Error(), reqUrl)
				return problemResp, entities.DPPageviewsResponse{}
			}
		}
		response.Items = append(response.Items, entities.DPPageviews{
			Project:       project,
			CountryCode:   country_code,
			Year:          year,
			Month:         month,
			Day:           day,
			PageID:        page_id,
			Delta:         delta,
			Epsilon:       epsilon,
			NoiseScale:    noise_scale,
			NoiseType:     noise_type,
			ReleaseThresh: release_thresh,
			Views:         views,
		})
	}

	str := "The date(s) you used are valid, but we either do not have data for those date(s), or the project you asked for is not loaded yet.  Please check documentation for more information."
	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateProblem(http.StatusNotFound, str, reqUrl)
		return problemResp, entities.DPPageviewsResponse{}
	}
	if err := scanner.Err(); err != nil {
		rLogger.Log(logger.ERROR, "Error querying database: %s", err)
		problemResp := aqsassist.CreateProblem(http.StatusInternalServerError, err.Error(), reqUrl)
		return problemResp, entities.DPPageviewsResponse{}
	}
	return problemData, response
}
