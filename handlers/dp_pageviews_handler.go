package handlers

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"

	"dp-pageviews/configuration"
	"dp-pageviews/logic"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

// DPPageviewsHandler is the HTTP handler for dp pageview endpoint requests.
type DPPageviewsHandler struct {
	Logger  *logger.Logger
	Session *gocql.Session
	Logic   logic.DPPageviewsLogicInterface
	Config  *configuration.Config
}

// API documentation
//
//		@summary		Get daily pageviews per per project, country, day, and page ID
//		@router			/dp-pageviews/{project}/{country_code}/{year}/{month}/{day}/{page_id}  [get]
//		@description	Given a Wikimedia project, country, day, and page_id or page_title, returns the number of pageviews that page got in that country on that day.
//		@param			project			path	string	true	"Domain of a Wikimedia project"				example(en.wikipedia.org)
//		@param			country_code	path	string	true	"Country to filter to"						example(US)
//		@param			year			path	string	true	"Year in YYYY format"						example(2023)
//		@param			month			path	string	true	"Month in MM format"						example(02)
//		@param			day				path	string	true	"Day in DD format"							example(09)
//	 	@param			page_id			path	string	true	"Numeric page ID"							example(12345)
//		@produce		json
//		@success		200	{object}	entities.DPPageviewsResponse
//		@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//		@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//		@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *DPPageviewsHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	project := aqsassist.TrimProjectDomain(vars["project"])
	country_code := strings.ToUpper(vars["country_code"])
	year := strings.ToLower(vars["year"])
	month := strings.ToLower(vars["month"])
	day := strings.ToLower(vars["day"])
	page_id := vars["page_id"]

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	var err error

	if !aqsassist.IsYear(year) {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Year should be in YYYY format between 2017 and present", reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsMonth(month) {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Month should be in MM format between 01 and 12", reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsDay(year, month, day, false) {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Day should be in DD format and valid for the year and month", reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if aqsassist.GetTime(year, month, day).Before(time.Date(2017, 2, 9, 0, 0, 0, 0, time.UTC)) {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Data not accessible via API prior to 9 Feb 2017. To access data from 1 Jul 2015-9 Feb 2017, visit https://w.wiki/7ePf.", reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if _, err := strconv.Atoi(page_id); err != nil {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Page ID should be an integer, and shouldn't contain any letters or punctuation marks", reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	c := context.Background()

	pbm, response := s.Logic.ProcessDPPageviewsLogic(c, reqUrl, project, country_code, year, month, day, page_id, s.Session, reqLogger)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	var data []byte
	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		reqLogger.Log(logger.ERROR, "Unable to marshal response object: %s", err)
		problemResp := aqsassist.CreateProblem(http.StatusInternalServerError, err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)
}
