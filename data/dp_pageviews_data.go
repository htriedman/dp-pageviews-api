package data

import (
	"context"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"

	"github.com/gocql/gocql"
)

type DPPageviewsDataInterface interface {
	ProcessDPPageviewsQuery(context context.Context, project, country_code, year, month, day, page_id string, session *gocql.Session, rLogger *logger.RequestScopedLogger) gocql.Scanner
}

type DPPageviewsData struct {
}

func (s DPPageviewsData) ProcessDPPageviewsQuery(context context.Context, project, country_code, year, month, day, page_id string, session *gocql.Session, rLogger *logger.RequestScopedLogger) gocql.Scanner {
	// Ensuring valid input for country, year, month, day
	country_code = country_code[0:2]
	year = year[0:4]
	month = month[0:2]
	day = day[0:2]
	query := `SELECT delta, epsilon, noise_scale, noise_type, release_thresh, views FROM aqs.dp_pageviews WHERE project = ? AND country_code = ? AND year = ? AND month = ? AND day = ? AND page_id = ?`
	scanner := session.Query(query, project, country_code, year, month, day, page_id).WithContext(context).Iter().Scanner()
	return scanner
}
