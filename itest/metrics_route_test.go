package itest

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestValidMetricsRoute(t *testing.T) {
	t.Run("should return 200 for metrics route", func(t *testing.T) {
		var specUrl = "http://localhost:8080/metrics"
		res, err := http.Get(specUrl)

		require.NoError(t, err, "Invalid http request")
		require.Equal(t, http.StatusOK, res.StatusCode, "Wrong status code")
	})
}
