package itest

// TODO: rewrite tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"

	"dp-pageviews/entities"
	"net/http"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

func testURL(suffix string) string {
	var fallback = "http://localhost:8080/metrics/dp-pageviews"
	var res string
	if res = os.Getenv("API_URL"); res == "" {
		return fmt.Sprintf("%s/%s", fallback, suffix)
	}
	return fmt.Sprintf("%s/%s", strings.TrimRight(res, "/"), suffix)
}

func runQuery(t *testing.T, project, country, year, month, day, page_id string) entities.DPPageviewsResponse {

	res, err := http.Get(testURL(fmt.Sprintf("%s/%s/%s/%s/%s/%s", project, country, year, month, day, page_id)))

	require.NoError(t, err, "Invalid http request")

	require.Equal(t, http.StatusOK, res.StatusCode, "Wrong status code")

	body, err := io.ReadAll(res.Body)
	require.NoError(t, err, "Unable to read response")

	n := entities.DPPageviewsResponse{}
	err = json.Unmarshal(body, &n)

	require.NoError(t, err, "Unable to unmarshal response body")
	return n
}

func TestDPPageviews(t *testing.T) {
	t.Run("should return 404 for an invalid route", func(t *testing.T) {
		// Leading slash should result in an invalid url
		res, err := http.Get(testURL("/en.wikipedia.org/US/2023/01/01/123"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusNotFound, res.StatusCode, "Wrong status code")
	})

	t.Run("should return 200 for expected parameters with page_id", func(t *testing.T) {

		res, err := http.Get(testURL("fr.wikipedia.org/FR/2023/07/24/11508644"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusOK, res.StatusCode, "Wrong status code")
	})

	t.Run("should return 400 when parameters are wrong", func(t *testing.T) {

		res, err := http.Get(testURL("wrong-project/wrong-country/1990/0000/0000"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")
	})

	t.Run("should return 400 when date is before 2015 July 1", func(t *testing.T) {

		res, err := http.Get(testURL("fr.wikipedia.org/FR/2017/02/08/11508644"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")

	})

	t.Run("should return 400 when year is invalid", func(t *testing.T) {

		res, err := http.Get(testURL("fr.wikipedia.org/FR/0000/07/24/11508644"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")

	})

	t.Run("should return 400 when month is invalid", func(t *testing.T) {

		res, err := http.Get(testURL("fr.wikipedia.org/FR/2023/00/24/11508644"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")

	})

	t.Run("should return 400 when day is invalid", func(t *testing.T) {

		res, err := http.Get(testURL("fr.wikipedia.org/FR/2023/07/00/11508644"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")

	})

	t.Run("should return 400 when page_id is invalid", func(t *testing.T) {

		res, err := http.Get(testURL("fr.wikipedia.org/FR/2023/07/24/abc123"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")

	})

	t.Run("should return 404 for invalid route", func(t *testing.T) {

		res, err := http.Get(testURL("fr.wikipedia.org/.invalid/FR/2023/07/24/11508644"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusNotFound, res.StatusCode, "Wrong status code")
	})

	t.Run("should return the correct data", func(t *testing.T) {

		n := runQuery(t, "fr.wikipedia.org", "FR", "2023", "07", "24", "12702509")

		assert.Len(t, n.Items, 1, "Unexpected response length")

		u := entities.DPPageviews{
			Project:       "fr.wikipedia",
			CountryCode:   "FR",
			Year:          "2023",
			Month:         "07",
			Day:           "24",
			PageID:        "12702509",
			Delta:         "0.0000001",
			Epsilon:       "1",
			NoiseScale:    "10",
			NoiseType:     "gaussian",
			ReleaseThresh: "90",
			Views:         118,
		}

		assert.Equal(t, n.Items[0], u, "Wrong contents")

	})

	t.Run("should return 405 for invalid HTTP verb", func(t *testing.T) {

		jsonBody := []byte(``)
		bodyReader := bytes.NewReader(jsonBody)

		res, err := http.Post(testURL("fr.wikipedia.org/FR/2023/07/24/12702509"), "application/json; charset=utf-8", bodyReader)

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusMethodNotAllowed, res.StatusCode, "Wrong status code")
	})
}

func TestSecurityHeaders(t *testing.T) {
	response, error := http.Get(testURL("fr.wikipedia.org/FR/2023/07/24/12702509"))
	require.NoError(t, error, "Invalid http request")
	t.Run("All security headers should be properly set", func(t *testing.T) {
		for headerName, headerValue := range aqsassist.HeadersToAdd {
			assert.Equal(t, headerValue, response.Header.Get(headerName), headerName+" should be set to "+headerValue)
		}
		assert.NotEmpty(t, response.Header.Get("Etag"), "Etag header should not be empty.")
	})
}
