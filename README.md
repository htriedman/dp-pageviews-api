# dp-pageviews-api

The DP pageviews API is a service (originally cloned from Device Analytics) that provides a public API developed and maintained by the Wikimedia Foundation that serves the [DP pageviews dataset](https://analytics.wikimedia.org/published/datasets/country_project_page/00_README.html), which contains the differentially-private number of pageviews for Wikipedia pages, partitioned over country, project, page_id, and day.

For more information, read the [docs on meta][wikipage].

### Docker Quickstart

You will need:
- [aqs-docker-test-env](https://gitlab.wikimedia.org/frankie/aqs-docker-test-env) and its associated dependencies

Start up the Dockerized test environment in aqs-docker-test-env and load 

```sh-session
make startup
```

then:

```sh-session
go run .
```
Then, connect to `http://localhost:8080/`.

## Unit Testing

To run a suite of unit tests, first start up the Dockerized test environment in aqs-docker-test-env, then:

```sh-session
make test
```

## API documentation

To update the API docs, run:

```
go install github.com/swaggo/swag/cmd/swag@latest

make docs
```

[wikipage]: https://meta.wikimedia.org/wiki/Differential_privacy
