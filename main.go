package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"path"
	"strings"

	"dp-pageviews/configuration"
	"dp-pageviews/data"
	"dp-pageviews/handlers"
	"dp-pageviews/logic"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	prometheusmiddleware "github.com/albertogviana/prometheus-middleware"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	// These values are assigned at build using `-ldflags` (see: Makefile)
	buildDate = "unknown"
	buildHost = "unknown"
	version   = "unknown"
)

// API documentation
//	@title					Wikimedia DP Pageivews API
//	@version				DRAFT
//	@description.markdown	api.md
//	@contact.name
//	@contact.url
//	@contact.email
//	@termsOfService	https://wikimediafoundation.org/wiki/Terms_of_Use
//	@host			localhost:8080
//	@basePath		/metrics/
//	@schemes		http

// Entrypoint for the service
func main() {
	var confFile = flag.String("config", "./config.yaml", "Path to the configuration file")

	var config *configuration.Config
	var err error
	var dpPageviewsData data.DPPageviewsData
	var dpPageviewsLogic = logic.NewDPPageviewsLogic(dpPageviewsData)
	flag.Parse()

	if config, err = configuration.ReadConfig(*confFile); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	logger, err := log.NewLogger(os.Stdout, config.ServiceName, config.LogLevel)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to initialize the logger: %s", err)
		os.Exit(1)
	}

	// Allow overriding config using environment variables
	MergeEnvironment(config, logger)

	logger.Info("Initializing service %s (Go version: %s, Build host: %s, Timestamp: %s", config.ServiceName, version, buildHost, buildDate)

	logger.Info("Connecting to Cassandra database(s): %s (port %d)", strings.Join(config.Cassandra.Hosts, ","), config.Cassandra.Port)
	logger.Debug("Cassandra: configured for consistency level '%s'", strings.ToLower(config.Cassandra.Consistency))
	logger.Debug("Cassandra: configured for local datacenter '%s'", config.Cassandra.LocalDC)

	session, err := newCassandraSession(config)
	if err != nil {
		logger.Error("Failed to create Cassandra session: %s", err)
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	// pass bound struct method to handler
	dpPageviewsHandler := &handlers.DPPageviewsHandler{
		Logger: logger, Session: session, Logic: &dpPageviewsLogic, Config: config}

	apiSpecHandler := &handlers.ApiSpecHandler{
		Logger: logger}

	healthz := NewHealthz(version, buildDate, buildHost)
	middleware := prometheusmiddleware.NewPrometheusMiddleware(prometheusmiddleware.Opts{})

	r := mux.NewRouter().SkipClean(true).UseEncodedPath()
	r.NotFoundHandler = http.HandlerFunc(handlers.NotFoundHandler)
	p := promhttp.Handler()

	r.Use(SetSecurityHeaders, middleware.InstrumentHandlerDuration)

	r.Handle("/metrics", p).Methods("GET")
	r.HandleFunc("/healthz", healthz.HandleHTTP).Methods("GET")
	r.HandleFunc("/dp-pageviews/api-spec.json", apiSpecHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/{project}/{country_code}/{year}/{month}/{day}/{page_id}"), dpPageviewsHandler.HandleHTTP).Methods("GET")

	srv := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", config.Address, config.Port),
		Handler: r,
	}

	err = srv.ListenAndServe()
	fmt.Println(err.Error())
}
