package entities

// DPPageviewsResponse represents a container for the DP pageviews resultset.
type DPPageviewsResponse struct {
	Items []DPPageviews `json:"items"`
}

// DPPageviews represents one result from the DP pageviews resultset.
type DPPageviews struct {
	Project       string `json:"project" example:"en.wikipedia.org"` // Wikimedia project domain
	CountryCode   string `json:"country_code" example:"DE"`          // Country
	Year          string `json:"year" example:"2023"`                // Year in YYYY format
	Month         string `json:"month" example:"12"`                 // Month in MM format
	Day           string `json:"day" example:"15"`                   // Day in DD format
	PageID        string `json:"page_id" example:"12345"`            // Numeric page ID
	Delta         string `json:"delta" example:"0.00001"`            // DP parameter used for Gaussian noise
	Epsilon       string `json:"epsilon" example:"1"`                // DP parameter known as the privacy budget
	NoiseScale    string `json:"noise_scale" example:"300"`          // DP parameter indicating the magnitude of noise added to data
	NoiseType     string `json:"noise_type" example:"Laplace"`       // DP parameter indicating the kind of noise added to data
	ReleaseThresh string `json:"release_thresh" example:"90"`        // DP parameter indicating minimum data value below which data was not released
	Views         int    `json:"views" example:"1234"`               // Number of views of page in country on YYYY-MM-DD
}
