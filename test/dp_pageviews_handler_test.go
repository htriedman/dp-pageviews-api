package test

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"testing"

	"dp-pageviews/configuration"
	"dp-pageviews/entities"
	"dp-pageviews/handlers"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"gitlab.wikimedia.org/frankie/aqsassist"

	"net/http/httptest"

	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"schneider.vip/problem"
)

const remoteAddr = "127.0.0.1:8080"

type MockDPPageviewsLogic struct {
	mock.Mock
}

func (m *MockDPPageviewsLogic) ProcessDPPageviewsLogic(context context.Context, reqUrl, project, country_code, year, month, day, page_id string, session *gocql.Session, rLogger *log.RequestScopedLogger) (*problem.Problem, entities.DPPageviewsResponse) {
	args := m.Called(context, reqUrl, project, country_code, year, month, day, page_id, session, rLogger)
	return args.Get(0).(*problem.Problem), args.Get(1).(entities.DPPageviewsResponse)
}

func testURL(suffix string) string {
	var fallback = "http://localhost:8080/metrics/dp-pageviews"
	var res string
	if res = os.Getenv("API_URL"); res == "" {
		return fmt.Sprintf("%s/%s", fallback, suffix)
	}
	return fmt.Sprintf("%s/%s", strings.TrimRight(res, "/"), suffix)
}

func TestDPPageviewsHandler_HandleHTTP(t *testing.T) {
	project := "en.wikipedia.org"
	country_code := "US"
	year := "2023"
	month := "01"
	day := "01"
	page_id := "123"
	var pbm = (*problem.Problem)(nil)

	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockResponse := entities.DPPageviewsResponse{
		Items: []entities.DPPageviews{
			{
				Project:       project,
				CountryCode:   country_code,
				Year:          year,
				Month:         month,
				Day:           day,
				PageID:        page_id,
				Delta:         "0.00001",
				Epsilon:       "1",
				NoiseScale:    "10",
				NoiseType:     "gaussian",
				ReleaseThresh: "100",
				Views:         100,
			}},
	}

	mockLogic := new(MockDPPageviewsLogic)

	handler := &handlers.DPPageviewsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/en.wikipedia.org/US/2023/01/01/123"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/en.wikipedia.org/US/2023/01/01/123")
	req = mux.SetURLVars(req, map[string]string{
		"project":      project,
		"country_code": country_code,
		"year":         year,
		"month":        month,
		"day":          day,
		"page_id":      page_id,
	})

	ctx := context.Background()

	mockLogic.On("ProcessDPPageviewsLogic", ctx, req.URL.RequestURI(), aqsassist.TrimProjectDomain(project), country_code, year, month, day, page_id, session, mockLogger.Request(req)).
		Return(pbm, mockResponse)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	var respData entities.DPPageviewsResponse
	err = json.Unmarshal(rec.Body.Bytes(), &respData)
	assert.NoError(t, err)
	assert.Equal(t, mockResponse, respData)

	mockLogic.AssertCalled(t, "ProcessDPPageviewsLogic", ctx, req.URL.RequestURI(), aqsassist.TrimProjectDomain(project), country_code, year, month, day, page_id, session, mockLogger.Request(req))
}

func TestDPPageviewsHandler_HandleHTTP_ErrorFromLogic(t *testing.T) {
	project := "en.wikipedia.org"
	country_code := "US"
	year := "2023"
	month := "01"
	day := "01"
	page_id := "123"
	ctx := context.Background()

	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockLogic := new(MockDPPageviewsLogic)

	handler := &handlers.DPPageviewsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/en.wikipedia.org/US/2023/01/01/123"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/en.wikipedia.org/US/2023/01/01/123")
	req = mux.SetURLVars(req, map[string]string{
		"project":      project,
		"country_code": country_code,
		"year":         year,
		"month":        month,
		"day":          day,
		"page_id":      page_id,
	})
	reqUri := req.URL.RequestURI()

	mockError := aqsassist.CreateProblem(http.StatusInternalServerError, "Query failed!", reqUri)

	mockLogic.On("ProcessDPPageviewsLogic", ctx, reqUri, aqsassist.TrimProjectDomain(project), country_code, year, month, day, page_id, session, mockLogger.Request(req)).
		Return(mockError, entities.DPPageviewsResponse{}).Once()

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusInternalServerError, rec.Code)

	assert.Equal(t, mockError.JSONString(), rec.Body.String())

	mockLogic.AssertCalled(t, "ProcessDPPageviewsLogic", ctx, reqUri, aqsassist.TrimProjectDomain(project), country_code, year, month, day, page_id, session, mockLogger.Request(req))
}

func TestDPPageviewsHandler_HandleHTTP_InvalidEarlyDate(t *testing.T) {
	project := "en.wikipedia.org"
	country_code := "US"
	year := "2015"
	month := "01"
	day := "01"
	page_id := "123"

	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockLogic := new(MockDPPageviewsLogic)

	handler := &handlers.DPPageviewsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/en.wikipedia.org/US/2015/01/01/123"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/en.wikipedia.org/US/2015/01/01/123")
	req = mux.SetURLVars(req, map[string]string{
		"project":      project,
		"country_code": country_code,
		"year":         year,
		"month":        month,
		"day":          day,
		"page_id":      page_id,
	})
	reqUri := req.URL.RequestURI()

	error := aqsassist.CreateProblem(http.StatusBadRequest, "Data not accessible via API prior to 9 Feb 2017. To access data from 1 Jul 2015-9 Feb 2017, visit https://w.wiki/7ePf.", reqUri)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, error.JSONString(), rec.Body.String())

}

func TestDPPageviewsHandler_HandleHTTP_InvalidYear(t *testing.T) {
	project := "en.wikipedia.org"
	country_code := "US"
	year := "invalid"
	month := "01"
	day := "01"
	page_id := "123"

	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockLogic := new(MockDPPageviewsLogic)

	handler := &handlers.DPPageviewsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/en.wikipedia.org/US/invalid/01/01/123"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/en.wikipedia.org/US/invalid/01/01/123")
	req = mux.SetURLVars(req, map[string]string{
		"project":      project,
		"country_code": country_code,
		"year":         year,
		"month":        month,
		"day":          day,
		"page_id":      page_id,
	})
	reqUri := req.URL.RequestURI()

	error := aqsassist.CreateProblem(http.StatusBadRequest, "Year should be in YYYY format between 2017 and present", reqUri)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, error.JSONString(), rec.Body.String())

}

func TestDPPageviewsHandler_HandleHTTP_InvalidMonth(t *testing.T) {
	project := "en.wikipedia.org"
	country_code := "US"
	year := "2018"
	month := "invalid"
	day := "01"
	page_id := "123"

	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockLogic := new(MockDPPageviewsLogic)

	handler := &handlers.DPPageviewsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/en.wikipedia.org/US/2018/invalid/01/123"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/en.wikipedia.org/US/2018/invalid/01/123")
	req = mux.SetURLVars(req, map[string]string{
		"project":      project,
		"country_code": country_code,
		"year":         year,
		"month":        month,
		"day":          day,
		"page_id":      page_id,
	})
	reqUri := req.URL.RequestURI()

	error := aqsassist.CreateProblem(http.StatusBadRequest, "Month should be in MM format between 01 and 12", reqUri)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, error.JSONString(), rec.Body.String())

}

func TestDPPageviewsHandler_HandleHTTP_InvalidDay(t *testing.T) {
	project := "en.wikipedia.org"
	country_code := "US"
	year := "2018"
	month := "01"
	day := "invalid"
	page_id := "123"

	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockLogic := new(MockDPPageviewsLogic)

	handler := &handlers.DPPageviewsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/en.wikipedia.org/US/2018/01/invalid/123"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/en.wikipedia.org/US/2018/01/invalid/123")
	req = mux.SetURLVars(req, map[string]string{
		"project":      project,
		"country_code": country_code,
		"year":         year,
		"month":        month,
		"day":          day,
		"page_id":      page_id,
	})
	reqUri := req.URL.RequestURI()

	error := aqsassist.CreateProblem(http.StatusBadRequest, "Day should be in DD format and valid for the year and month", reqUri)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, error.JSONString(), rec.Body.String())

}

func TestDPPageviewsHandler_HandleHTTP_Invalid_PageID(t *testing.T) {
	project := "en.wikipedia.org"
	country_code := "US"
	year := "2018"
	month := "01"
	day := "01"
	page_id := "abc123"

	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockLogic := new(MockDPPageviewsLogic)

	handler := &handlers.DPPageviewsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/en.wikipedia.org/US/2018/01/01/abc123"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/en.wikipedia.org/US/2018/01/invalid/abc123")
	req = mux.SetURLVars(req, map[string]string{
		"project":      project,
		"country_code": country_code,
		"year":         year,
		"month":        month,
		"day":          day,
		"page_id":      page_id,
	})
	reqUri := req.URL.RequestURI()

	error := aqsassist.CreateProblem(http.StatusBadRequest, "Page ID should be an integer, and shouldn't contain any letters or punctuation marks", reqUri)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, error.JSONString(), rec.Body.String())

}
