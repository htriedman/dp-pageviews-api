package test

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"reflect"
	"testing"

	"dp-pageviews/entities"
	"dp-pageviews/logic"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

type MockDPPageviewsData struct {
	mock.Mock
}

type MockScanner struct {
	mock.Mock
	data  [][]interface{}
	index int
}

func (m *MockScanner) SetData(data [][]interface{}) {
	m.data = data
	m.index = -1
}

func (m *MockScanner) Scan(dest ...interface{}) error {
	if m.index >= len(m.data) {
		return io.EOF
	}
	row := m.data[m.index]
	if len(dest) != len(row) {
		return fmt.Errorf("not found")
	}
	for i, val := range row {
		if val == nil {
			continue
		}
		valType := reflect.TypeOf(val)
		destType := reflect.TypeOf(dest[i]).Elem()
		if !valType.ConvertibleTo(destType) {
			return fmt.Errorf("cannot convert value of type %s to type %s", valType, destType)
		}
		destValue := reflect.ValueOf(dest[i]).Elem()
		destValue.Set(reflect.ValueOf(val).Convert(destType))
	}
	return nil

}

func (m *MockScanner) Err() error {
	// return an error if there was an error scanning values,
	// or return nil if no error occurred
	return nil
}

func (m *MockScanner) Next() bool {
	m.index++
	return m.index < len(m.data)

}

func (m *MockDPPageviewsData) ProcessDPPageviewsQuery(context context.Context, project, country_code, year, month, day, page_id string, session *gocql.Session, rLogger *log.RequestScopedLogger) gocql.Scanner {
	args := m.Called(context, project, country_code, year, month, day, page_id, session, rLogger)
	return args.Get(0).(gocql.Scanner)
}

func TestProcessDPPageviewsLogic_Success(t *testing.T) {
	// setup mock data
	mockData := &MockDPPageviewsData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}

	expectedResponse := entities.DPPageviewsResponse{
		Items: []entities.DPPageviews{
			{
				Project:       "testProject",
				CountryCode:   "testCountry",
				Year:          "year",
				Month:         "month",
				Day:           "day",
				PageID:        "page_id",
				Delta:         "0.00001",
				Epsilon:       "1",
				NoiseScale:    "10",
				NoiseType:     "laplace",
				ReleaseThresh: "123",
				Views:         456,
			},
		},
	}

	// setup expectations for ProcessDPPageviewsQuery call
	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{"0.00001", "1", "10", "laplace", "123", 456},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessDPPageviewsQuery", mock.Anything, "testProject", "testCountry", "year", "month", "day", "page_id", session, rLogger).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewDPPageviewsLogic(mockData)

	// call ProcessDPPageviewsLogic
	_, actualResponse := logic.ProcessDPPageviewsLogic(context.Background(), "", "testProject", "testCountry", "year", "month", "day", "page_id", session, rLogger)

	// assert response matches expected value
	assert.Equal(t, expectedResponse, actualResponse)

}

func TestProcessDPPageviewsLogic_NoData(t *testing.T) {
	// setup mock data
	mockData := &MockDPPageviewsData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}

	// setup expectations for ProcessDPPageviewsQuery call
	mockScanner := new(MockScanner)
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessDPPageviewsQuery", mock.Anything, "testProject", "testCountry", "year", "month", "day", "page_id", session, rLogger).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewDPPageviewsLogic(mockData)

	// call ProcessDPPageviewsLogic
	_, actualResponse := logic.ProcessDPPageviewsLogic(context.Background(), "", "testProject", "testCountry", "year", "month", "day", "page_id", session, rLogger)

	// assert response matches expected value
	assert.Equal(t, 0, len(actualResponse.Items))

}

func TestProcessDPPageviewsLogic_QueryError(t *testing.T) {
	// setup mock data
	mockData := &MockDPPageviewsData{}
	session := &gocql.Session{}
	req, err := http.NewRequest("GET", "localhost:8080/metrics/dp-pageviews/", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = "127.0.0.1:8080"
	logger, _ := log.NewLogger(os.Stdout, "", "error")
	rLogger := logger.Request(req)

	// setup expectations for ProcessDPPageviewsQuery call
	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{"0.00001", "1", "10", "laplace", 123},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)

	mockData.On("ProcessDPPageviewsQuery", mock.Anything, "testProject", "testCountry", "year", "month", "day", "page_id", session, rLogger).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewDPPageviewsLogic(mockData)

	// call ProcessDPPageviewsLogic
	pbmResp, actualResponse := logic.ProcessDPPageviewsLogic(context.Background(), "", "testProject", "testCountry", "year", "month", "day", "page_id", session, rLogger)

	// assert response matches expected value
	assert.Equal(t, 0, len(actualResponse.Items))
	assert.NotEmpty(t, pbmResp.Error())

}

func TestProcessDPPageviewsLogic_NotFound(t *testing.T) {
	// setup mock data
	mockData := &MockDPPageviewsData{}
	session := &gocql.Session{}
	req, err := http.NewRequest("GET", "localhost:8080/metrics/dp-pageviews/", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = "127.0.0.1:8080"
	logger, _ := log.NewLogger(os.Stdout, "", "error")
	rLogger := logger.Request(req)

	// setup expectations for ProcessDPPageviewsQuery call
	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{"0.00001", "1", "10", "laplace", 123},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("not found"))

	expectedProblem := aqsassist.CreateProblem(http.StatusNotFound, "The date(s) you used are valid, but we either do "+
		"not have data for those date(s), or the project you asked for is not loaded yet.  "+
		"Please check documentation for more information.", req.RequestURI)

	mockData.On("ProcessDPPageviewsQuery", mock.Anything, "testProject", "testCountry", "year", "month", "day", "page_id", session, rLogger).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewDPPageviewsLogic(mockData)

	// call ProcessDPPageviewsLogic
	pbmResp, actualResponse := logic.ProcessDPPageviewsLogic(context.Background(), "", "testProject", "testCountry", "year", "month", "day", "page_id", session, rLogger)

	// assert response matches expected value
	assert.Equal(t, 0, len(actualResponse.Items))
	assert.Equal(t, expectedProblem.Error(), pbmResp.Error())

}
