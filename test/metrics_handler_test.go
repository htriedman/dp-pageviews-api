package test

import (
	"io/ioutil"
	"net/http"
	"testing"

	"net/http/httptest"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/stretchr/testify/assert"
)

func TestMetricsHandler_HandleHTTP(t *testing.T) {
	metricsUrl := "http://localhost:8080/metrics"

	handler := promhttp.Handler()

	req, err := http.NewRequest("GET", metricsUrl, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = metricsUrl

	rec := httptest.NewRecorder()
	handler.ServeHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	responseBody, err := ioutil.ReadAll(rec.Body)
	assert.NoError(t, err, "Failed to read response body")
	assert.Greater(t, len(responseBody), 0)
}
