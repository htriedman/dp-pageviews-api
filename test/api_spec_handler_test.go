package test

import (
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"runtime"
	"testing"

	"dp-pageviews/handlers"

	"net/http/httptest"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"

	"github.com/stretchr/testify/assert"
)

func TestApiSpecHandler_HandleHTTP(t *testing.T) {
	specUrl := "http://localhost:8080/dp-pageviews/api-spec.json"
	mockLogger := &log.Logger{}

	handler := &handlers.ApiSpecHandler{
		Logger: mockLogger,
	}

	req, err := http.NewRequest("GET", specUrl, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = specUrl

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	responseBody, err := ioutil.ReadAll(rec.Body)
	assert.NoError(t, err, "Failed to read response body")

	//the working directory is relative to the test binary,
	// so a swaggerFilePath like "./docs/swagger.json" will not find the swagger spec
	//therefore getting working directory and trimming the suffix till it reaches service-name dir
	_, filename, _, _ := runtime.Caller(0)
	dir := path.Join(path.Dir(filename), "..")
	err = os.Chdir(dir)
	if err != nil {
		panic(err)
	}
	swaggerFilePath := "./docs/swagger.json"

	expectedSwaggerFile, err := ioutil.ReadFile(swaggerFilePath)
	assert.NoError(t, err, "Failed to read expected swagger file")

	assert.Equal(t, expectedSwaggerFile, responseBody, "Response body does not match expected swagger file contents")

}
