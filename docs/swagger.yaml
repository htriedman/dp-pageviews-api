basePath: /metrics/
definitions:
  entities.DPPageviews:
    properties:
      country_code:
        description: Country
        example: DE
        type: string
      day:
        description: Day in DD format
        example: "15"
        type: string
      delta:
        description: DP parameter used for Gaussian noise
        example: "0.00001"
        type: string
      epsilon:
        description: DP parameter known as the privacy budget
        example: "1"
        type: string
      month:
        description: Month in MM format
        example: "12"
        type: string
      noise_scale:
        description: DP parameter indicating the magnitude of noise added to data
        example: "300"
        type: string
      noise_type:
        description: DP parameter indicating the kind of noise added to data
        example: Laplace
        type: string
      page_id:
        description: Numeric page ID
        example: "12345"
        type: string
      project:
        description: Wikimedia project domain
        example: en.wikipedia.org
        type: string
      release_thresh:
        description: DP parameter indicating minimum data value below which data was
          not released
        example: "90"
        type: string
      views:
        description: Number of views of page in country on YYYY-MM-DD
        example: 1234
        type: integer
      year:
        description: Year in YYYY format
        example: "2023"
        type: string
    type: object
  entities.DPPageviewsResponse:
    properties:
      items:
        items:
          $ref: '#/definitions/entities.DPPageviews'
        type: array
    type: object
host: localhost:8080
info:
  contact: {}
  description: |
    *This page is a work in progress.*

    DP Pageviews is a service that provides a public API developed and maintained by the Wikimedia Foundation that serves analytical
    data about number of views that a given page ID got in a given country on a given day.

    Data provided by this API is available under the [CC0 1.0 license](https://creativecommons.org/publicdomain/zero/1.0/).
  termsOfService: https://wikimediafoundation.org/wiki/Terms_of_Use
  title: Wikimedia DP Pageivews API
  version: DRAFT
paths:
  /dp-pageviews/{project}/{country_code}/{year}/{month}/{day}/{page_id}:
    get:
      description: Given a Wikimedia project, country, day, and page_id or page_title,
        returns the number of pageviews that page got in that country on that day.
      parameters:
      - description: Domain of a Wikimedia project
        example: en.wikipedia.org
        in: path
        name: project
        required: true
        type: string
      - description: Country to filter to
        example: US
        in: path
        name: country_code
        required: true
        type: string
      - description: Year in YYYY format
        example: "2023"
        in: path
        name: year
        required: true
        type: string
      - description: Month in MM format
        example: "02"
        in: path
        name: month
        required: true
        type: string
      - description: Day in DD format
        example: "09"
        in: path
        name: day
        required: true
        type: string
      - description: Numeric page ID
        example: "12345"
        in: path
        name: page_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/entities.DPPageviewsResponse'
        "400":
          description: Bad request
          schema:
            properties:
              detail:
                type: string
              method:
                type: string
              status:
                type: integer
              title:
                type: string
              uri:
                type: string
            type: object
        "404":
          description: Not found
          schema:
            properties:
              detail:
                type: string
              method:
                type: string
              status:
                type: integer
              title:
                type: string
              uri:
                type: string
            type: object
        "500":
          description: Internal server error
          schema:
            properties:
              detail:
                type: string
              method:
                type: string
              status:
                type: integer
              title:
                type: string
              uri:
                type: string
            type: object
      summary: Get daily pageviews per per project, country, day, and page ID
schemes:
- http
swagger: "2.0"
