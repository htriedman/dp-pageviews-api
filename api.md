*This page is a work in progress.*

DP Pageviews is a service that provides a public API developed and maintained by the Wikimedia Foundation that serves analytical
data about number of views that a given page ID got in a given country on a given day.

Data provided by this API is available under the [CC0 1.0 license](https://creativecommons.org/publicdomain/zero/1.0/).
